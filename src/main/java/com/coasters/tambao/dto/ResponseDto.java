package com.coasters.tambao.dto;


import com.coasters.tambao.utils.ErrorCode;

public class ResponseDto {
    private Integer error_code;
    private String msg;
    private Object object;
    public static ResponseDto success(String msg,Object object){
        return new ResponseDto(ErrorCode.OPERATOR_SUCCESS,msg,object);
    }


    public static ResponseDto error(Integer error_code,String msg){
        return new ResponseDto(error_code,msg,null);
    }

    @Override
    public String toString() {
        return "ResponseDto{" +
                "error_code=" + error_code +
                ", msg='" + msg + '\'' +
                ", object=" + object +
                '}';
    }

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public ResponseDto() {
    }

    public ResponseDto(Integer error_code, String msg, Object object) {
        this.error_code = error_code;
        this.msg = msg;
        this.object = object;
    }
}
