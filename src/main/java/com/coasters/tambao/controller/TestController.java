package com.coasters.tambao.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Tamba
 * Created Date 2021/03/10 0010
 * Description:
 */
@Controller
public class TestController {

    @RequestMapping("helloworld")
    @ResponseBody
    public String helloworld(){
        return "helloworld!";
    }
}
