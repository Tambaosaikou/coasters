package com.coasters.tambao.controller;

import com.coasters.tambao.pojo.User;
import com.coasters.tambao.service.UserService;
import com.coasters.tambao.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Tamba
 * Created Date 2021/06/11 0011
 * Description: 用户
 */
@Controller
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "login",method = RequestMethod.POST)
    @ResponseBody
    public Object userLogin(@RequestParam String loginid,@RequestParam String password){
        return userService.userLogin(loginid,password);
    }

    @RequestMapping(value = "register",method = RequestMethod.POST)
    @ResponseBody
    public Object userRegister(@RequestParam String loginid,@RequestParam String password){
        return userService.userRegister(loginid,password);
    }

//    @RequestMapping(value = "/api/user/exist",method = RequestMethod.GET)
    @GetMapping(value = "exist")
    @ResponseBody
    public Object checkExist(@RequestParam String loginid){
        return userService.checkExist(loginid);
    }

    @PostMapping(value = "modify")
    @ResponseBody
    public Object modifyUserWithoutPassword(@RequestParam User user,@RequestParam int userid){
        return userService.modifyUserWithoutPassword(user,userid);
    }

    @GetMapping(value ="get")
    @ResponseBody
    public Object getUser(@RequestParam Integer userid){
        return userService.getUser(userid);
    }

    @PostMapping(value = "modifypwd")
    @ResponseBody
    public Object modifyPassword(@RequestParam Integer userid,@RequestParam String oldPassword,@RequestParam String newPassword){
        return userService.modifyPassword(userid,oldPassword,newPassword);
    }

}
