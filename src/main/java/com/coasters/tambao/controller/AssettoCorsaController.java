package com.coasters.tambao.controller;

import com.coasters.tambao.dto.ResponseDto;
import com.coasters.tambao.service.AssettoCorsaService;
import com.coasters.tambao.utils.ErrorCode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * Created by Tamba
 * Created Date 2021/06/17 0017
 * Description: 神力科莎功能主控制器
 */
@Controller
@RequestMapping(value = "/api/ac")
public class AssettoCorsaController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AssettoCorsaService acService;

    @RequestMapping(value = "createlink", method = RequestMethod.POST)
    @ResponseBody
    public Object createLink(@RequestParam(value = "ip", required = false) String ip, @RequestParam(value = "port", required = false, defaultValue = "8081") String port) {
        if (StringUtils.isNotEmpty(ip) && StringUtils.isNotEmpty(port)) {
            return acService.createLink(ip, port);
        }
        return ResponseDto.error(ErrorCode.AC_LINKCONFERROR, "IP或端口为空，请检查");
    }

    @RequestMapping(value = "getpubliclinklist", method = RequestMethod.GET)
    @ResponseBody
    public Object getPublicLinkList() {
        log.info("开始获取地址list");
        return acService.getPublicLinkList();
    }


}
