package com.coasters.tambao.service;

import com.coasters.tambao.dto.ResponseDto;

/**
 * Created by Tamba
 * Created Date 2021/06/18 0018
 * Description:
 */
public interface AssettoCorsaService {
    ResponseDto createLink(String ip, String port);

    ResponseDto getPublicLinkList();
}
