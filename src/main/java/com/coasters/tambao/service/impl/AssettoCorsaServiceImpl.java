package com.coasters.tambao.service.impl;

import com.coasters.tambao.constant.AssettoCorsa;
import com.coasters.tambao.dto.ResponseDto;
import com.coasters.tambao.mapper.AcAddressMapper;
import com.coasters.tambao.pojo.AcAddress;
import com.coasters.tambao.pojo.AcAddressExample;
import com.coasters.tambao.service.AssettoCorsaService;
import com.coasters.tambao.utils.ErrorCode;
import com.coasters.tambao.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Tamba
 * Created Date 2021/06/18 0018
 * Description:
 */
@Service
public class AssettoCorsaServiceImpl implements AssettoCorsaService {

    @Autowired
    private AcAddressMapper acAddressMapper;

    @Override
    public ResponseDto createLink(String ip, String port) {
        ResultVo res = new ResultVo();
        AcAddressExample acAddressExample = new AcAddressExample();
        AcAddress acAddress = new AcAddress();
        String ipTem = AssettoCorsa.addressTemplate;
        String[] ipTemArr = ipTem.split("\\*");
        int arrLenth = ipTemArr.length;
        StringBuilder link = new StringBuilder();
        if (arrLenth == 2) {
            link.append(ipTemArr[0]);
            link.append(ip);
            link.append(ipTemArr[1]);
            link.append(port);

            acAddress.setIpaddress(ip);
            acAddress.setPort(port);
            acAddress.setLink(link.toString());
            acAddress.setIsvalid(1);
            acAddress.setCreatetime(new Date());
            acAddress.setHaspassword(0);
            res.setCode(1);
            res.setMsg(link.toString());
            int count = acAddressMapper.insert(acAddress);
//            int count = acAddressMapper.insertSelective(acAddress);
            if (count > 0) {
                return ResponseDto.success("成功发布", res);
            } else {
                return ResponseDto.error(ErrorCode.DATABASE_INSERTERROR, "新建邀请链接失败");
            }
        } else {
            return ResponseDto.error(ErrorCode.AC_LINKCONFERROR, "配置文件错误，请检查程序设定");
        }
    }

    @Override
    public ResponseDto getPublicLinkList() {
        ResultVo res = new ResultVo();
        List<AcAddress> resList = new ArrayList<>();
        AcAddressExample acAddressExample = new AcAddressExample();
        acAddressExample.createCriteria().andIsvalidEqualTo(1);
        resList = acAddressMapper.selectByExample(acAddressExample);
        if (resList.size() > 0) {
            return ResponseDto.success("查询成功", resList);
        } else {
            return ResponseDto.error(ErrorCode.DATABASE_QUERYERROR, "查询失败");
        }
    }
}
