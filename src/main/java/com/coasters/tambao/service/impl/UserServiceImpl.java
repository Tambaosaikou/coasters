package com.coasters.tambao.service.impl;

import com.coasters.tambao.dto.ResponseDto;
import com.coasters.tambao.mapper.UserMapper;
import com.coasters.tambao.pojo.User;
import com.coasters.tambao.pojo.UserExample;
import com.coasters.tambao.service.UserService;
import com.coasters.tambao.utils.ErrorCode;
import com.coasters.tambao.utils.MD5Utils;
import org.apache.tomcat.util.security.MD5Encoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by Tamba
 * Created Date 2021/06/11 0011
 * Description:
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public ResponseDto userLogin(String loginid, String password) {
        password = MD5Utils.encrypt(password);
        User user =userMapper.userLogin(loginid,password);
        if (user == null) {
            return ResponseDto.error(ErrorCode.USER_ERROR,"用户名或密码错误");
        }else{
            return ResponseDto.success("登录成功",user);
        }
    }

    @Override
    public ResponseDto userRegister(String loginid, String password) {
        password = MD5Utils.encrypt(password);
        User user = new User();
        user.setLoginid(loginid);
        user.setPassword(password);
        int flag = userMapper.insertSelective(user);
        if (flag>0){
            return ResponseDto.success("注册成功",flag);
        }else{
            return ResponseDto.error(ErrorCode.USER_ERROR,"注册失败");
        }
    }

    @Override
    public ResponseDto checkExist(String loginid) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andLoginidEqualTo(loginid);
        Long count = userMapper.countByExample(userExample);
        if (count>0){
            return ResponseDto.error(ErrorCode.USER_ERROR,"用户登录名已存在");
        }else{
            return ResponseDto.success("校验通过",null);
        }
    }

    @Override
    public ResponseDto modifyUserWithoutPassword(User user,int userid) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andBirthdayEqualTo(user.getBirthday()).andUsernameEqualTo(user.getUsername()).andLastnameEqualTo(user.getLastname()).andSexEqualTo(user.getSex()).andMobileEqualTo(user.getMobile()).andModifieddateEqualTo(new Date()).andModifyuserEqualTo(String.valueOf(userid));
        int count = userMapper.updateByExampleSelective(user,userExample);
        if (count>0){
            return ResponseDto.success("更新成功",null);
        }else{
            return ResponseDto.error(ErrorCode.USER_ERROR,null);
        }
    }

    @Override
    public ResponseDto getUser(Integer userid) {
        User user = new User();
        user = userMapper.selectByPrimaryKey(userid);
        if (user==null){
            return ResponseDto.error(ErrorCode.USER_ERROR,"获取用户失败");
        }else{
            return ResponseDto.success("获取用户成功",user);
        }
    }

    @Override
    public ResponseDto modifyPassword(Integer userid, String oldPassword, String newPassword) {
        //先去判断老密码对不对
        User user = new User();
        user = userMapper.getPassword(userid);
        if (user==null){
            return ResponseDto.error(ErrorCode.OPERATOR_FAILED,"修改密码获取用户信息出错");
        }
        String encryptOldPassword = MD5Utils.encrypt(oldPassword);
        if (encryptOldPassword.equals(user.getPassword())){
            user.setPassword(MD5Utils.encrypt(newPassword));
            int count = userMapper.updateByPrimaryKeySelective(user);
            if (count>0){
                return ResponseDto.success("更新密码成功",null);
            }else{
                return ResponseDto.error(ErrorCode.USER_ERROR,"更新密码失败");
            }
        }else{
            return ResponseDto.error(ErrorCode.USER_ERROR,"旧密码验证失败");
        }
    }
}
