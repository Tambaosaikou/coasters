package com.coasters.tambao.service;

import com.coasters.tambao.dto.ResponseDto;
import com.coasters.tambao.pojo.User;
import org.springframework.stereotype.Service;

/**
 * Created by Tamba
 * Created Date 2021/06/11 0011
 * Description:
 */
public interface UserService {
    ResponseDto userLogin(String loginid, String password);

    ResponseDto userRegister(String loginid, String password);

    ResponseDto checkExist(String loginid);

    ResponseDto modifyUserWithoutPassword(User user,int userid);

    ResponseDto getUser(Integer userid);

    ResponseDto modifyPassword(Integer userid, String oldPassword, String newPassword);
}
