package com.coasters.tambao.mapper;

import com.coasters.tambao.pojo.AcAddress;
import com.coasters.tambao.pojo.AcAddressExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AcAddressMapper {
    long countByExample(AcAddressExample example);

    int deleteByExample(AcAddressExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(AcAddress record);

    int insertSelective(AcAddress record);

    List<AcAddress> selectByExample(AcAddressExample example);

    AcAddress selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") AcAddress record, @Param("example") AcAddressExample example);

    int updateByExample(@Param("record") AcAddress record, @Param("example") AcAddressExample example);

    int updateByPrimaryKeySelective(AcAddress record);

    int updateByPrimaryKey(AcAddress record);
}