package com.coasters.tambao.utils;

public class ErrorCode {
    public static final Integer OPERATOR_SUCCESS = 1; //操作成功！
    public static final Integer OPERATOR_FAILED = 0;//操作失败

    public static final Integer DATABASE_INSERTERROR = 11;//数据库插入失败
    public static final Integer DATABASE_QUERYERROR = 12;//数据库查询失败


    public static final Integer USER_ERROR = 1001;//用户相关错误

    public static final Integer AC_LINKCONFERROR = 2001;//AC配置文件错误

}
