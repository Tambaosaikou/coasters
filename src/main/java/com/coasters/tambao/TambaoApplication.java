package com.coasters.tambao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.coasters.tambao.mapper")
public class TambaoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TambaoApplication.class, args);
    }

}
